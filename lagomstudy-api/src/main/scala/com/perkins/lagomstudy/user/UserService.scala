package com.perkins.lagomstudy.user

import akka.NotUsed
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}

trait UserService extends Service {

  def listUser(id: String): ServiceCall[NotUsed, List[String]]

  override final def descriptor: Descriptor = {
    import Service._
    named("lagomstudy")
      .withCalls(
        pathCall("/user/:id", listUser _)
      )
      .withAutoAcl(true)
  }
}
