package com.perkins.lagomstudy.user

import com.lightbend.lagom.scaladsl.api.ServiceCall

import scala.concurrent.Future

class UserServiceImpl extends UserService {
  override def listUser(id: String) = ServiceCall { _ =>
    Future.successful(List.empty[String])
  }
}
