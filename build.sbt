organization in ThisBuild := "com.perkins"
version in ThisBuild := "1.0-SNAPSHOT"

// the Scala version that will be used for cross-compiled libraries
scalaVersion in ThisBuild := "2.13.0"

val macwire = "com.softwaremill.macwire" %% "macros" % "2.3.3" % "provided"
val scalaTest = "org.scalatest" %% "scalatest" % "3.0.8" % Test

lazy val `lagomstudy` = (project in file("."))
  .aggregate(`lagomstudy-api`, `lagomstudy-impl`, `lagomstudy-stream-api`, `lagomstudy-stream-impl`)

lazy val `lagomstudy-api` = (project in file("lagomstudy-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )

lazy val `lagomstudy-impl` = (project in file("lagomstudy-impl"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceCassandra,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      macwire,
      scalaTest
    )
  )
  .settings(lagomForkedTestSettings)
  .dependsOn(`lagomstudy-api`)
  .settings(lagomServiceAddress := "0.0.0.0") //指定该微服务绑定的网卡
  .settings(lagomServiceHttpPort := 11000) //指定该我服务监听的端口。如果这里不指定，则随机选取一个端口


lazy val `lagomstudy-stream-api` = (project in file("lagomstudy-stream-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    )
  )

lazy val `lagomstudy-stream-impl` = (project in file("lagomstudy-stream-impl"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslTestKit,
      macwire,
      scalaTest
    )
  )
  .dependsOn(`lagomstudy-stream-api`, `lagomstudy-api`)

//SBT 优化配置。https://mp.weixin.qq.com/s/KC_my3H4big7eH2zWqb2lA
offline := true
updateOptions := updateOptions.value.withCachedResolution(true).withLatestSnapshots(false)


lagomCassandraPort in ThisBuild := 9042  // 取消 Cassandra server 4000的默认端口  。https://www.lagomframework.com/documentation/1.4.x/java/CassandraServer.html#Default-port
lagomServiceGatewayAddress in ThisBuild := "0.0.0.0" //指定 gateway 绑定的网卡
lagomServiceGatewayPort in ThisBuild := 9010 //指定gateway监听的端口，默认是9000
// Implementation of the service gateway: "akka-http" (default) or "netty"
lagomServiceGatewayImpl in ThisBuild := "netty" //默认是"akka-http" 可指定为 "netty"
lagomServiceLocatorEnabled in ThisBuild := true //关闭 gateway 服务




/**
  * 启动命令：
  *     全部启动：sbt runAll(依赖 Cassandra server)
  *     单个服务启动: projectName/run （该项目中未执行成功）  https://www.lagomframework.com/documentation/1.6.x/scala/IntroGetStarted.html
  * 发布到本地：publishLocal
  *
  *
  */

