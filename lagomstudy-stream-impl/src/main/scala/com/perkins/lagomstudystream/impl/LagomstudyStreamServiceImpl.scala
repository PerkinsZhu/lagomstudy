package com.perkins.lagomstudystream.impl

import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.perkins.lagomstudystream.api.LagomstudyStreamService
import com.perkins.lagomstudy.api.LagomstudyService

import scala.concurrent.Future

/**
  * Implementation of the LagomstudyStreamService.
  */
class LagomstudyStreamServiceImpl(lagomstudyService: LagomstudyService) extends LagomstudyStreamService {
  def stream = ServiceCall { hellos =>
    Future.successful(hellos.mapAsync(8)(lagomstudyService.hello(_).invoke()))
  }
}
