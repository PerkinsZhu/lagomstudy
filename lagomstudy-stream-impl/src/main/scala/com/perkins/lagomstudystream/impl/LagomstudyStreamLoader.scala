package com.perkins.lagomstudystream.impl

import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.server._
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import play.api.libs.ws.ahc.AhcWSComponents
import com.perkins.lagomstudystream.api.LagomstudyStreamService
import com.perkins.lagomstudy.api.LagomstudyService
import com.softwaremill.macwire._

class LagomstudyStreamLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new LagomstudyStreamApplication(context) {
      override def serviceLocator: NoServiceLocator.type = NoServiceLocator
    }

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new LagomstudyStreamApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[LagomstudyStreamService])
}

abstract class LagomstudyStreamApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with AhcWSComponents {

  // Bind the service that this server provides
  override lazy val lagomServer: LagomServer = serverFor[LagomstudyStreamService](wire[LagomstudyStreamServiceImpl])

  // Bind the LagomstudyService client
  lazy val lagomstudyService: LagomstudyService = serviceClient.implement[LagomstudyService]
}
